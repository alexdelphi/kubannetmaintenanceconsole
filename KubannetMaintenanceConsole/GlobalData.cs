﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KubannetMaintenanceConsole
{
    public class GlobalData
    {
        public static string DateToUri(DateTime date)
        {
            return date.ToString("yyyy.M.d.H.m.s");
        }
        /// <summary>
        /// Строка формирует URI для просмотра статуса соединения на основе текущего времени и даты
        /// </summary>
        /// <returns>Сформированный URI</returns>
        public static string StatusURI(DateTime date)
        {
            return "http://192.168.0.50/conninfo.xgi?r=" + DateToUri(date);
        }
        public const int RequestTimeout = 5000;
        public const string LoginCredentials = "ACTION_POST=LOGIN&FILECODE=&VERIFICATION_CODE=&LOGIN_USER=admin&LOGIN_PASSWD=&login=Log+In+&VER_CODE=";
        public static string ConnectURI(DateTime date)
        {
            return StatusURI(date) + "&set/runtime/wan/inf:1/connect=1";
        }
        public static string DisconnectURI(DateTime date)
        {
            return StatusURI(date) + "&set/runtime/wan/inf:1/disconnect=1";
        }
        public const string LoginURI = "http://192.168.0.50/login.php";
        public const string LogoutURI = "http://192.168.0.50/logout.php";
    }
}
