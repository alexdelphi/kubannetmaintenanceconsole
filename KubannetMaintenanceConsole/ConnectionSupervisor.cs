﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Threading.Tasks;

namespace KubannetMaintenanceConsole
{
    public class ConnectionSupervisor
    {
        /// <summary>
        /// помогает разобрать ответ 
        /// </summary>
        private static Dictionary<string, ConnectionStatus> ConnectionArray = new Dictionary<string, ConnectionStatus>()
        {
            {" \"connected\"", ConnectionStatus.Connected},
            {" \"connecting\"", ConnectionStatus.Connecting},
            {" \"disconnecting\"", ConnectionStatus.Disconnecting},
            {" \"disconnected\"", ConnectionStatus.Disconnected},
            {" \"\"", ConnectionStatus.Unknown}
        };
        public void Login()
        {
            using (var webClient = new WebClient())
            {
                try
                {
                    webClient.UploadString(new Uri(GlobalData.LoginURI), GlobalData.LoginCredentials);
                }
                catch (WebException e)
                {
                    System.Windows.Forms.MessageBox.Show(e.Message + Environment.NewLine + e.StackTrace);
                }
                //await webClient.UploadStringTaskAsync(new Uri(GlobalData.LoginURI), GlobalData.LoginCredentials);
            }
        }
        /// <summary>
        /// Разбор ответа (см. коммент к PerformAction())
        /// </summary>
        /// <param name="rawAnswer"></param>
        /// <returns></returns>
        public static ConnectionStatus ExtractStatus(string rawAnswer)
        {
            var status = rawAnswer.Split(new[] { ',' })[1];
            return ConnectionArray[status];
        }
        /// <summary>
        /// получение данных от роутера, ответ приходит в формате
        /// var result = new Array("OK", "disconnected", "212.192.143.219", "255.255.255.255", "212.192.138.149", "212.192.128.4&nbsp212.192.128.3", "");
        /// </summary>
        /// <param name="url">Откуда получаем данные</param>
        /// <returns></returns>
        public async Task<ConnectionStatus> PerformActionAsync(string url)
        {
            var req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = "GET";
            /* HttpWebResponse response = null;
             try
             {
                 response = await req.GetResponseAsync() as HttpWebResponse;
             }
             catch(Exception e)
             {
                 using(var writer = new StreamWriter(@"c:\temp.txt", true)) // append
                 {
                     writer.WriteLine(e.Message + Environment.NewLine + e.StackTrace);
                     writer.Close();
                 }                
             }*/
            //HttpWebResponse response = await req.GetResponseAsync() as HttpWebResponse;
            //  System.Windows.Forms.MessageBox.Show(".!.");
            HttpWebResponse response = null;
            try
            {
                //response = req.GetResponse() as HttpWebResponse;
                response = await req.GetResponseAsync() as HttpWebResponse;
            }
            catch (WebException ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
            }
            //System.Windows.Forms.MessageBox.Show(".?.");
            string str = await new StreamReader(response.GetResponseStream()).ReadLineAsync();
            return ExtractStatus(str);
        }
        public ConnectionStatus PerformAction(string url, bool ResultNeeded = true, int Timeout = GlobalData.RequestTimeout)
        {
            var req = (HttpWebRequest)WebRequest.Create(url);
            req.Timeout = Timeout;
            req.Method = "GET";
            HttpWebResponse response = req.GetResponse() as HttpWebResponse;
            if(ResultNeeded)
            {
                try
                {
                    string str = new StreamReader(response.GetResponseStream()).ReadLine();
                    return ExtractStatus(str);
                }
                catch (NullReferenceException e)
                {
                    return ConnectionStatus.Unknown;
                }
            }
            return ConnectionStatus.Unknown;
                //response = await req.GetResponseAsync() as HttpWebResponse;
        }
        public Task<ConnectionStatus> ConnectAsync()
        {
            return PerformActionAsync(GlobalData.ConnectURI(DateTime.Now));
        }
        public ConnectionStatus Connect()
        {
            return PerformAction(GlobalData.ConnectURI(DateTime.Now), false);
        }
        public Task<ConnectionStatus> DisconnectAsync()
        {
            return PerformActionAsync(GlobalData.DisconnectURI(DateTime.Now));
        }
        public ConnectionStatus Disconnect()
        {
            return PerformAction(GlobalData.DisconnectURI(DateTime.Now), false);
        }
        public async Task LogoutAsync()
        {
            using (var webClient = new WebClient())
            {
                await webClient.DownloadDataTaskAsync(new Uri(GlobalData.LogoutURI));
            }
        }
        public void Logout()
        {
            using (var webClient = new WebClient())
            {
                webClient.DownloadData(new Uri(GlobalData.LogoutURI));
            }
        }
        /// <summary>
        /// Получить текущий статус
        /// </summary>
        /// <returns></returns>
        public Task<ConnectionStatus> GetStatusAsync()
        {
            return PerformActionAsync(GlobalData.StatusURI(DateTime.Now));
        }
        public ConnectionStatus GetStatus()
        {
            return PerformAction(GlobalData.StatusURI(DateTime.Now), true);
        }
    }
}
