﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace KubannetMaintenanceConsole
{
    public enum ConnectionStatus
    {
        Connecting,
        Connected,
        Disconnecting,
        Disconnected,
        Unknown
//	, TestMyHui
    }
}
