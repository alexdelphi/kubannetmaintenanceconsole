﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Net;
using System.IO;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace KubannetMaintenanceConsole
{
   
    class Program
    {
        private static ConnectionSupervisor cs = new ConnectionSupervisor();
        private static RNGCryptoServiceProvider random = new RNGCryptoServiceProvider();
        private static Random rnd = new Random();
        private static StreamWriter writer = new StreamWriter("1.txt");
        private static bool isTryingToConnect;
        private static Timer TimerPing = new Timer()
        {
            Interval = 1000,
            Enabled = false
        };
        private static Timer TimerConnect = new Timer()
        {
            Interval = 5000,
            Enabled = false
        };
        static MyResponse pingfunc()
        {
            Ping pingSender = new Ping();
            PingOptions options = new PingOptions();

            // Use the default Ttl value which is 128,
            // but change the fragmentation behavior.
            options.DontFragment = false;
            options.Ttl = 128;//60;


            // Create a buffer of 32 bytes of data to be transmitted.
            string data = "a"; //"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            int timeout = 1000;
            string host;
            // host = "wlc.campus.kubsu.ru";
            host = "www.ya.ru";
            PingReply reply;
            try
            {
                reply = pingSender.Send(host, timeout, buffer, options);
                if (reply.Status == IPStatus.Success)
                {
                    // System.Console.WriteLine("Ping to " + host + " = " + reply.RoundtripTime.ToString());
                    Console.WriteLine("Ping to " + host + " = " + reply.RoundtripTime.ToString());
                }
                else if (reply.Status == IPStatus.TimedOut)
                {
                    //System.Console.WriteLine("Ping to " + host + " " + reply.Status.ToString() + "(" + timeout + ")");
                    Console.WriteLine("Ping to " + host + " " + reply.Status.ToString() + "(" + timeout + ")");
                }
                else
                {
                    //System.Console.WriteLine("Ping to " + host + " " + reply.Status.ToString());
                    Console.WriteLine("Ping to " + host + " " + reply.Status.ToString() + "(" + timeout + ")");
                    //  MessageBox.Show("Ping to " + host + " " + reply.Status.ToString() + "(" + timeout + ")");
                }
                //  return reply.Status == IPStatus.Success;
                return new MyResponse("Ping to " + host + " " + reply.Status.ToString() + "(" + timeout + ")", reply.Status == IPStatus.Success);
            }
            catch (PingException err)
            {
                //Беда. надо восстанавливать сеть. Тут можно и проверку статус вклеить.

                //MessageBox.Show( "Ping to " + host + " = Генерал Филюра к вашим услугам!");
                Console.WriteLine("Ping to " + host + " = Генерал Филюра к вашим услугам!");
                //System.Console.WriteLine("Ping to " + host + " = Генерал Филюра к вашим услугам!");
                return new MyResponse("Ping to " + host + " = Генерал Филюра к вашим услугам!", false);
            }
        }
        static byte[] LockObject()
        {
            byte[] res = new byte[32];
            random.GetBytes(res);
            return res;
        }
        static void Main(string[] args)
        {
            TimerPing.Elapsed += (obj, sender) =>
            {
                // the same thread can enter the critical section

                MyResponse PingResult = pingfunc();

                /* Если таймаут не прошел, запросим статус роутера. Если он disconnected/disconnecting, проверка и ребут */
                //int newInterval = 15000;
                //TimerPing.Interval = (TimerPing.Interval == newInterval) ? OldInterval : TimerPing.Interval;
                if (!PingResult.Flag)
                {
                    //Console.WriteLine("Пинг не успешен, пытаемся переподключиться...");
                    TimerPing.Stop();
                    TimerConnect.Start();
                }
            };
            TimerConnect.Elapsed += (obj, sender) =>
            {
                lock (rnd.NextDouble().ToString()) // lock(LockObject())
                {
                    ConnectionStatus status = ConnectionStatus.Unknown;
                    Console.WriteLine("Соединение не обнаружено");
                    //cs.Login();   // здесь молча падаем, если сети нет
                    //try
                    //{
                    try
                    {
                        status = cs.GetStatus();
                    }
                    catch (WebException e)
                    {
                        if ((e.Response != null) && (e.Response as HttpWebResponse).StatusCode == HttpStatusCode.Unauthorized)
                        {
                            Console.WriteLine("Выполняется повторная авторизация...");
                            cs.Login();
                        }
                        else
                        {
                            Console.WriteLine("Ошибка получения статуса: " + e.Message);
                        }
                        writer.WriteLine(DateTime.Now.ToString() + " " + e.Message + Environment.NewLine + e.StackTrace);
                        writer.Flush();
                    }
                    //}
                    //catch (Exception e)
                    //{
                    //    writer.WriteLine(e.Message);
                    //    writer.WriteLine(e.Source);
                    //    writer.Flush();
                    //    Console.WriteLine("Получение статуса не удалось");
                    //}
                    Console.WriteLine("Текущий статус: " + status.ToString());
                    if ((status == ConnectionStatus.Disconnected) || (status == ConnectionStatus.Disconnecting))
                    {
                        Console.WriteLine("Пытаемся подключиться");
                        try
                        {
                            //lock(rnd.NextDouble().ToString())
                            //{
                            //    if(!isTryingToConnect)
                            //    {
                                    isTryingToConnect = true;
                                    cs.Connect();
                                    isTryingToConnect = false; 
                                    Console.WriteLine("Подключение выполнено успешно");
                                    TimerConnect.Stop();
                                    TimerPing.Start();
                            //    }
                            //    else
                            //    {
                            //        Console.WriteLine("Подключение уже выполняется");
                            //    }
                            //}

                            // спим
                            // System.Threading.Thread.Sleep(5000);
                        }
                        catch (WebException e)
                        {
                            if ((e.Response != null) && (e.Response as HttpWebResponse).StatusCode == HttpStatusCode.Unauthorized)
                            {
                                Console.WriteLine("Выполняется повторная авторизация...");     
                                cs.Login();
                                //cs.Connect();
                                //Console.WriteLine("Подключение выполнено успешно");
                            }
                            else
                            {
                                Console.WriteLine("Ошибка подключения: " + e.Message);
                            }
                            writer.WriteLine(DateTime.Now.ToString() + " " + e.Message);
                            writer.WriteLine(e.StackTrace);
                            writer.Flush();
                            //System.Windows.Forms.MessageBox.Show(e.Message);
                        }
                        
                        //TimerPing.Stop();
                        ////TimerConnect_Tick(this, new EventArgs());
                        //TimerConnect.Start();
                    }
                    else
                    {
                        Console.WriteLine("Подключение не требуется");
                        TimerConnect.Stop();
                        TimerPing.Start();
                    }
                    
                }
            };
            TimerPing.Start();
            System.Windows.Forms.Application.Run();
        }
    }
    class MyResponse
    {
        public string Message;
        public bool Flag;
        public MyResponse()
        {
            this.Message = "";
            this.Flag = false;

        }
        public MyResponse(string mess, bool flag)
        {
            this.Message = mess;
            this.Flag = flag;

        }
    }
}
